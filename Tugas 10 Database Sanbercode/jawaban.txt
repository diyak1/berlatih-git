1. Buat Database
	
CREATE DATABASE myshop;

2. Membuat Table
   a). users

	CREATE TABLE users(
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT null,
    email varchar(255) NOT null,
    password varchar(255) NOT null
);

   b). categories

	CREATE TABLE categories(
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT null
);

   c). items

	CREATE TABLE items(
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT null,
    description varchar(255) NOT null,
    price int(11) not null,
    stock int(11) not null,
    category_id int(11),
    FOREIGN KEY(category_id) REFERENCES categories(id)
);

3. Memasukkan Data
   a). users

	INSERT INTO users(name, email, password) VALUES ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123")
;
   b). categories

	INSERT INTO categories(name) VALUES ("gadeget"), ("cloth"), ("men"), ("women"), ("branded");

   c). items

	INSERT INTO items(name, description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4. Mengambil Data
   a). Data user

	SELECT id, name, email FROM users;

   b). Data Items

	SELECT * FROM items WHERE price > 1000000;

	SELECT * FROM items WHERE name LIKE "%watch";

   c). Tampil data Items

	SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name FROM items INNER JOIN categories ON items.category_id = categories.id;

5. Mengubah data dari database

	UPDATE items SET price = 2500000 WHERE id = 1;