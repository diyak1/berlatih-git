<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Form()
    {
        return view('form');
    }

    public function welcome(Request $request)
    {
        $firstname = $request['namadepan'];
        $lastname = $request['namabelakang'];

        return view('welcome', compact('firstname', 'lastname'));
    }
}
