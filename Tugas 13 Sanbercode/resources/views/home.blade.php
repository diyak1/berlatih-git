@extends('layout.master')
@section('judul')
SanberBook
@endsection
@section('content')
<h2>Social Media Developer Santai Berkualitas</h2>
<p class="paragraph">Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
<h3>Benefit Join di Sanberbook</h3>
<ul>
    <li>Mendapatkan motivasi dari sesama developer</li>
    <li>Sharing knowledge dari para mastah Sanber</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>
<h3>Cara bergabung ke SanberBook</h3>
<ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftar di
        <a href="/form">Form Sign Up</a>
    </li>
    <li>Selesai!</li>
</ol>
@endsection 